#"MiniNero" by Shen Noether mrl. Use at your own risk.
import hashlib #for signatures
import math
#import Crypto.Random.random as rand
import Keccak #cn_fast_hash
#import mnemonic #making 25 word mnemonic to remember your keys
import binascii #conversion between hex, int, and binary. Also for the crc32 thing
import ed25519 #Bernsteins python ed25519 code from cr.yp.to
import ed25519ietf # https://tools.ietf.org/html/draft-josefsson-eddsa-ed25519-02
import zlib

b = 256
q = 2**255 - 19
l = 2**252 + 27742317777372353535851937790883648493

def intToHex(i):
    return binascii.hexlify(ed25519.encodeint(i)) #hexlify does bytes to hex

def mul8(point):
    return binascii.hexlify(scalarmult_simple(point, 8))


def scalarmult_simple(pk, num):
    #returns point encoded to hex.. num is an int, not a hex
    return ed25519.encodepoint(ed25519.scalarmult(toPoint(pk), num)) #pub key is not just x coord..

def toPoint(hexVal):
    aa = binascii.unhexlify(hexVal) #to binary (new)
    return ed25519.decodepoint(aa) #make to point



def toPointCheck(hexVal):
    aa = binascii.unhexlify(hexVal) #to binary (new)
    return ed25519.decodepointcheck(aa) #make to point

def hexToInt(h):
    s = binascii.unhexlify(h) #does hex to bytes
    bb = len(h) * 4 #I guess 8 bits / b
    return sum(2**i * ed25519.bit(s,i) for i in range(0,bb)) #does to int


def cn_fast_hash(s):
    #len(hex(s)) # mozna tohle pomuze
    k = Keccak.Keccak()
    return k.Keccak((len(s) * 4, s), 1088, 512, 32 * 8, False).lower() #r = bitrate = 1088, c = capacity, n = output length in bits



def hashToPointCN(hexVal):
    u = hexToInt(cn_fast_hash(hexVal)) % q
    A = 486662
    ma = -1 * A % q
    ma2 = -1 * A * A % q
    sqrtm1 = ed25519.sqroot(-1)
    d = ed25519.theD() #print(radix255(d))
    fffb1 = -1 * ed25519.sqroot(-2 * A * (A + 2) )
    #print("fffb1", ed25519.radix255(fffb1))
    fffb2 = -1 * ed25519.sqroot(2 * A * (A + 2) )
    #print("fffb2", ed25519.radix255(fffb2))
    fffb3 = ed25519.sqroot( -1 * sqrtm1 * A * (A + 2))
    #print("fffb3", ed25519.radix255(fffb3))
    fffb4 = -1 * ed25519.sqroot( sqrtm1 * A * (A + 2))
    #print("fffb4", ed25519.radix255(fffb4))

    w = (2 * u * u + 1) % q
    xp = (w *  w - 2 * A * A * u * u) % q

    #like sqrt (w / x) although may have to check signs..
    #so, note that if a squareroot exists, then clearly a square exists..
    rx = ed25519.expmod(w * ed25519.inv(xp),(q+3)/8,q) 
    #rx is ok. 

    x = rx * rx * (w * w - 2 * A * A * u * u) % q

    y = (2 * u * u  + 1 - x) % q #w - x, if y is zero, then x = w

    negative = False
    if (y != 0):
        y = (w + x) % q #checking if you got the negative square root.
        if (y != 0) :
            negative = True
        else :
            rx = rx * -1 * ed25519.sqroot(-2 * A * (A + 2) ) % q
            negative = False
    else :
        #y was 0..
        rx = (rx * -1 * ed25519.sqroot(2 * A * (A + 2) ) ) % q 
    if not negative:
        rx = (rx * u) % q
        z = (-2 * A * u * u)  % q
        sign = 0
    else:
        z = -1 * A
        x = x * sqrtm1 % q #..
        y = (w - x) % q 
        if (y != 0) :
            rx = rx * ed25519.sqroot( -1 * sqrtm1 * A * (A + 2)) % q
        else :
            rx = rx * -1 * ed25519.sqroot( sqrtm1 * A * (A + 2)) % q
        sign = 1
    #setsign
    if ( (rx % 2) != sign ):
        rx =  - (rx) % q 
    rz = (z + w) % q
    ry = (z - w) % q
    rx = rx * rz % q

    P = ed25519ietf.point_compress([rx, ry, rz])
    P8 = mul8(P)
    toPointCheck(P)
    return P8

def scalarmultBase(sk):
    #returns pubkey in hex, expects hex sk
    return binascii.hexlify(public_key(hexToInt(sk)))

################################
################################
def scalarmultKeyInt(pk, num):
   return binascii.hexlify(scalarmult_simple(pk, num))
################################
################################

def scalarmultKey(pk, num):
   return binascii.hexlify(scalarmult_simple(pk, hexToInt(num)))

def keyImage(x):
    HP = hashToPointCN(scalarmultBase(x))
    return scalarmultKey(HP, x)

def public_key(sk):
    #returns point encoded to binary .. sk is just an int..
    return ed25519.encodepoint(ed25519.scalarmultbase(sk)) #pub key is not just x coord..


if __name__ == "__main__":

  # Output's private key
  X = format(0x39f60ab67a49c086d5c31a37cd503fdf0867b57410e7df370a981a1828b0790e, 'x')
  print("Computing key image: ")
  print("Output's private key: ")
  print(X)
  print("Key image:           ")
  print(keyImage(X))
  

